/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Portatil
 */
public class GroupNumbersTest {
    private int[] vectorWithPositiveNumbers;
    private int[] vectorWithNegativeNumbers;
    private int numberOfGroups;
    public GroupNumbersTest() {
    }
   
    
    @Before
    public void setUp() {
        
        vectorWithPositiveNumbers = new int[]{16,15,14,13,34,23,24,25,26,
                            28,45,34,23,29,12,23,45,67,
                            23,12,34,45,23,67,23,67
                           };
        
        vectorWithNegativeNumbers = new int[]{16,15,14,13,34,23,24,25,26,
                            28,45,34,23,29,-12,23,45,67,
                            23,12,34,45,23,67,-23,67
                           };
        
        numberOfGroups = 3;
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test groupNumbersByProximityWithMoreGroupsThanNumbers method, of class GroupNumbers
     */
    @Test (expected = IllegalArgumentException.class)
    public void testGroupNumbersByProximityWithMoreGroupsThanNumbers(){
        
        System.out.println("Number of groups should not be greater than vector length!");
        
        GroupNumbers instance = new GroupNumbers();
        
        numberOfGroups = 50;
        
        instance.groupNumbersByProximity(vectorWithPositiveNumbers, numberOfGroups);
        
    }
    
    
    /**
     * Test groupNumbersByProximityWithLessThanOneGroup method, of class GroupNumbers
     */
    @Test (expected = IllegalArgumentException.class)
    public void testGroupNumbersByProximityWithLessThanOneGroup(){
        
        System.out.println("Number of groups should not be less than 2!");
        
        GroupNumbers instance = new GroupNumbers();
        
        numberOfGroups = 0;
        
        instance.groupNumbersByProximity(vectorWithPositiveNumbers, numberOfGroups);
        
    }
    
    
    /**
     * Test of groupNumbersByProximityWithThreeGroups method, of class GroupNumbers.
     */
    @Test
    public void testGroupNumbersByProximityWithThreeGroups() {
        
        System.out.println("Group Numbers By Proximity with positive numbers!");
      
        GroupNumbers instance = new GroupNumbers();
        
        Map<Integer, List<Integer>> expResult = new HashMap<>(); 
        List<Integer> group = new ArrayList<>();

        group.add(16); group.add(15); group.add(14); group.add(13); group.add(34); group.add(23); group.add(24); 
        group.add(25); group.add(26); group.add(28); group.add(34); group.add(23); group.add(29); group.add(12); 
        group.add(23); group.add(23); group.add(12); group.add(34); group.add(23); group.add(23);
        expResult.put(0, group);
        
        group = new ArrayList<>(); 
        group.add(45); group.add(45); group.add(45);
        expResult.put(1, group);
        
        group = new ArrayList<>();
        group.add(67); group.add(67); group.add(67);
        expResult.put(2, group);

        Map<Integer, List<Integer>> result = instance.groupNumbersByProximity(vectorWithPositiveNumbers, numberOfGroups);
        assertEquals(expResult, result);
    }
    
    
    /**
     * Test of groupNumbersByProximityWithThreeGroupsAndNegativeNumbers method, of class GroupNumbers.
     */
    @Test
    public void testGroupNumbersByProximityWithThreeGroupsAndNegativeNumbers() {
        
        System.out.println("Group Numbers By Proximity with negative numbers!");
      
        GroupNumbers instance = new GroupNumbers();
        
        Map<Integer, List<Integer>> expResult = new HashMap<>(); 
        List<Integer> group = new ArrayList<>();

        
        group.add(-12);group.add(-23);
        expResult.put(0, group);
        
        group = new ArrayList<>();
        group.add(16); group.add(15); group.add(14); group.add(13); group.add(34); group.add(23); group.add(24); group.add(25); group.add(26); 
        group.add(28); group.add(45); group.add(34); group.add(23); group.add(29); group.add(23); group.add(45); group.add(23); group.add(12); 
        group.add(34); group.add(45); group.add(23);
        expResult.put(1, group);
       
        group = new ArrayList<>();
        group.add(67);  group.add(67); group.add(67);
        expResult.put(2, group);
        
        Map<Integer, List<Integer>> result = instance.groupNumbersByProximity(vectorWithNegativeNumbers, numberOfGroups);
        assertEquals(expResult, result);
    }
    
}
