package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Diogo Tavares
 */
public class GroupNumbers {

    /**
     * given an array of numbers, group numbers by proximity
     *
     * @param vector an array of numbers
     * @param numberOfGroups number of groups
     * @return a map with numbers grouped by proximity
     */
    public Map<Integer, List<Integer>> groupNumbersByProximity(int[] vector, int numberOfGroups) {

        Map<Integer, List<Integer>> groups = new HashMap<>();
        List<Integer> group = new ArrayList<>();
        int i, j, indice, limitNumber, inferiorLimit;

        if (isValidNumberOfGroups(vector, numberOfGroups)) {

            int[] sortedArray = sortArray(vector);

            int[] auxVec = distanceToNextNumber(sortedArray);

            int[] biggestDistances = biggestDistances(auxVec, numberOfGroups);

            biggestDistances = sortBiggestDistances(biggestDistances);

            int[] limits = limitValuesOfVector(vector);
            inferiorLimit = limits[0];

            for (i = 0; i < numberOfGroups; i++) {
                if (i == (numberOfGroups - 1)) {
                    indice = limits[1] + 1;
                    limitNumber = indice + 1;
                } else {
                    indice = biggestDistances[i] + 1;
                    limitNumber = sortedArray[indice];

                }
                for (j = 0; j < vector.length; j++) {
                    if (vector[j] >= inferiorLimit && vector[j] < limitNumber) {
                        group.add(vector[j]);
                    }
                }
                groups.put(i, group);
                group = new ArrayList<>();
                inferiorLimit = limitNumber;
            }
        }
        return groups;
    }

    /**
     * validate the number of groups
     *
     * @param vector array with numbers
     * @param numberOfGroups number of groups
     * @return true is a valid number of groups or an exception if is not
     */
    private Boolean isValidNumberOfGroups(int[] vector, int numberOfGroups) {
        int vectorLength;

        if (numberOfGroups < 2) {
            throw new IllegalArgumentException("The number of groups are less than 2.");
        }

        vectorLength = vector.length;

        if (numberOfGroups > vectorLength) {
            throw new IllegalArgumentException("The number of groups are greater than number of numbers in vector.");
        }

        return true;
    }

    /**
     * ascending sort of a given array
     *
     * @param vector array of numbers
     * @return array sorted
     */
    private int[] sortArray(int[] vector) {
        int i, j, tmp;

        int[] auxVec = new int[vector.length];

        for (i = 0; i < vector.length; i++) {
            auxVec[i] = vector[i];
        }

        for (i = 0; i < vector.length; i++) {
            for (j = i + 1; j < vector.length - 1; j++) {
                if (auxVec[i] > auxVec[j]) {
                    tmp = auxVec[j];
                    auxVec[j] = auxVec[i];
                    auxVec[i] = tmp;
                }
            }
        }
        return auxVec;
    }

    /**
     * calculate distance between numbers
     *
     * @param sortedArray sorted array with numbers
     * @return an array with distances between numbers
     */
    private int[] distanceToNextNumber(int[] sortedArray) {
        int i;
        int[] auxVec = new int[sortedArray.length];

        for (i = 0; i < sortedArray.length - 1; i++) {
            auxVec[i] = sortedArray[i + 1] - sortedArray[i];
        }
        return auxVec;
    }

    /**
     * get the indexes of biggest differences between numbers of an array
     *
     * @param auxVec array with numbers
     * @param numberOfGroups number of groups
     * @return an array with indexes of biggest differences between numbers
     */
    private int[] biggestDistances(int[] auxVec, int numberOfGroups) {
        int[] biggestDistances = new int[numberOfGroups - 1];
        int i, j, max = 0, index;
        int[] auxVec2 = new int[auxVec.length];

        for (i = 0; i < auxVec.length; i++) {
            auxVec2[i] = auxVec[i];
        }

        for (j = 0; j < (numberOfGroups - 1); j++) {
            max = auxVec2[0];
            index = 0;
            for (i = 1; i < auxVec2.length; i++) {
                if (max < auxVec2[i]) {
                    max = auxVec2[i];
                    index = i;
                }
            }
            biggestDistances[j] = index;
            auxVec2[index] = Integer.MIN_VALUE;
        }
        return biggestDistances;
    }

    /**
     * sort elements by ascending order
     *
     * @param biggestDistances array with biggest distances indexes
     * @return an array with biggest distances sorted
     */
    private int[] sortBiggestDistances(int[] biggestDistances) {

        if (biggestDistances[0] > biggestDistances[1]) {
            int tmp = biggestDistances[1];
            biggestDistances[1] = biggestDistances[0];
            biggestDistances[0] = tmp;
        }

        return biggestDistances;
    }

    /**
     * get the minimum and maximum numbers of an array
     *
     * @param vector array with numbers
     * @return an array with min and max values
     */
    private int[] limitValuesOfVector(int[] vector) {
        int i, maxNumber = Integer.MIN_VALUE, minNumber = Integer.MAX_VALUE;
        int vectorLength = vector.length;
        int[] limits = new int[2];

        for (i = 0; i < vectorLength; i++) {
            if (vector[i] > maxNumber) {
                maxNumber = vector[i];
            }
            if (vector[i] < minNumber) {
                minNumber = vector[i];
            }
        }
        limits[0] = minNumber;
        limits[1] = maxNumber;

        return limits;
    }
}
